<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Entry;
use AppBundle\Form\EntryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/{id}", name="add_edit_action", defaults={"id": null}, requirements={"id": "\d+"})
     * @ParamConverter("entry", class="AppBundle:Entry")
     *
     * @param Request    $request
     * @param Entry|null $entry
     *
     * @return Response
     */
    public function indexAction(Request $request, Entry $entry = null)
    {
        $requestedEntry = $request->attributes->getInt('id');

        if ($requestedEntry > 0 && !$entry) {
            throw $this->createNotFoundException();
        }

        $entry = $entry ?? new Entry();

        $form = $this->createForm(EntryType::class, $entry);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entry);
            $entityManager->flush();

            $this->addFlash('success', 'Entry added!');

            return $this->redirectToRoute('list_action');
        }

        $params = [
            'form' => $form->createView()
        ];
        return $this->render('default/index.html.twig', $params);
    }

    /**
     * @Route("/list", name="list_action")
     */
    public function listAction()
    {
        $repo = $this->getDoctrine()->getRepository(Entry::class);
        $entries = $repo->findAll();

        return $this->render(
            'default/list.html.twig',['entries' => $entries]);
    }

    /**
     * @Route("/delete/{id}", name="delete_action", methods={"GET"})
     * @ParamConverter("entry", class="AppBundle:Entry")
     * @param Entry $entry
     *
     * @return RedirectResponse
     */
    public function deleteAction(Entry $entry)
    {
        $entityManager = $this->getDoctrine()->getManager();
        if ($entry->getPicture()) {
            unlink( $entry->getPicture());
        }
        $entityManager->remove($entry);
        $entityManager->flush();

        $this->addFlash('success', 'Entry deleted');
        return $this->redirectToRoute('list_action');
    }

}
