<?php

namespace AppBundle\Form;

use AppBundle\Entity\Entry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextareaType::class)
            ->add('lastname', TextareaType::class)
            ->add('address1', TextareaType::class)
            ->add('zip', TextareaType::class)
            ->add('city', TextareaType::class)
            ->add('country', CountryType::class)
            ->add('phone', TextareaType::class)
            ->add('birthday', DateType::class)
            ->add('email', EmailType::class)
            ->add('picture', FileType::class, ['required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entry::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'csrf_token_id'   => 'entry_item',
        ]);
    }
}
